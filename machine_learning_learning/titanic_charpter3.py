from common import load_titanic_data, print_with_separator
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
import numpy as np
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score


train_set, test_set = load_titanic_data()

print_with_separator(train_set.info())
print_with_separator(train_set.describe())

cat_attribs = ["Sex", "Embarked"]
num_attribs = ["Pclass", "Age", "SibSp", "Parch", "Fare"]

X_train, y_train = (
    train_set.drop("Survived", axis=1)
    .drop("Name", axis=1)
    .drop("Cabin", axis=1)
    .drop("Ticket", axis=1),
    train_set["Survived"].copy(),
)

X_test = test_set.drop("Name", axis=1).drop("Cabin", axis=1).drop("Ticket", axis=1)

num_pipeline = Pipeline(
    [("imputer", SimpleImputer(strategy="median")), ("std_scaler", StandardScaler())]
)

text_pipeline = Pipeline(
    [("imputer", SimpleImputer(strategy="most_frequent")), ("cat", OneHotEncoder())]
)

full_pipeline = ColumnTransformer(
    [("num", num_pipeline, num_attribs), ("cat", text_pipeline, cat_attribs)]
)
prepared = full_pipeline.fit_transform(X_train)

# Now we train models

tree_clf = RandomForestClassifier()
tree_clf.fit(prepared, y_train)

svm_clf = SVC()
svm_clf.fit(prepared, y_train)

# Measuring error

tree_score = cross_val_score(tree_clf, prepared, y_train, cv=10, scoring="accuracy")
print("Tree Reg RMSE: ", tree_score.mean())

svm_score = cross_val_score(svm_clf, prepared, y_train, cv=10, scoring="accuracy")
print("SVM Reg RMSE: ", svm_score.mean())

prepared = full_pipeline.transform(X_test)
rui = svm_clf.predict([prepared[0]])
print(str(rui))
