import pandas as pd
import numpy as np
from learner import Learner, TopFeatureSelector
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import cross_val_score, GridSearchCV, RandomizedSearchCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.svm import SVR
from common import print_with_separator, load_housing_data, fetch_housing_data
from common import display_scores
from scipy.stats import expon, reciprocal, randint


fetch_housing_data()
housing = load_housing_data()
housing_back = housing
housing["income_cat"] = pd.cut(
    housing["median_income"],
    bins=[0.0, 1.5, 3.0, 4.5, 6.0, np.inf],
    labels=[1, 2, 3, 4, 5],
)
print_with_separator(housing.head())
print_with_separator(housing.info())
housing.hist(bins=50, figsize=(20, 15))
# plt.pyplot.show()
learner = Learner(housing, "income_cat")
print(type(learner.train_set))

# Divinding the test set in labels and attributes
housing = learner.train_set.drop("median_house_value", axis=1)
housing_labels = learner.train_set["median_house_value"].copy()

# The Imputer only supports numerical values so we need to take
# ocean_proximity out of the dataset
imputer = SimpleImputer(strategy="median")
housing_num = housing.drop("ocean_proximity", axis=1)
imputer.fit(housing_num)
print_with_separator(imputer.statistics_)


# Filling the missing values
X = imputer.transform(housing_num)
housing_num_transformed = pd.DataFrame(
    X, columns=housing_num.columns, index=housing_num.index
)
print_with_separator(housing_num_transformed.info())


# The text attribute still needs to be enconded into a numerical attribute
housing_cat = housing[["ocean_proximity"]]
cat_enconder = OneHotEncoder()
housing_cat_1hot = cat_enconder.fit_transform(housing_cat)
print_with_separator(housing_cat_1hot.toarray())
print_with_separator(cat_enconder.categories_)


# All This pre processing can be done using a pipeline to simplify
num_pipeline = Pipeline(
    [("imputer", SimpleImputer(strategy="median")), ("std_scaler", StandardScaler())]
)
num_attribs = list(housing_num)
cat_attribs = ["ocean_proximity"]

full_pipeline = ColumnTransformer(
    [("num", num_pipeline, num_attribs), ("cat", OneHotEncoder(), cat_attribs)]
)
housing_prepared = full_pipeline.fit_transform(housing)

###############
# Excercise 1 #
###############
# Question: Try a Support Vector Machine regressor (sklearn.svm.SVR), with various
# hyperparameters such as kernel="linear" (with various values for the C hyperparameter)
# or kernel="rbf" (with various values for the C and gamma hyperparameters). Don't worry
# about what these hyperparameters mean for now. How does the best SVR predictor perform?

param_grid = [
    {"kernel": ["linear"], "C": [10.0, 300.0, 3000.0, 30000.0]},
    {
        "kernel": ["rbf"],
        "C": [1.0, 3.0, 30.0, 300.0, 1000.0],
        "gamma": [0.01, 0.1, 0.3, 1.0, 3.0],
    },
]

grid_search = GridSearchCV(
    SVR(), param_grid, n_jobs=24, cv=2, scoring="neg_mean_squared_error", verbose=0
)

grid_search.fit(housing_prepared, housing_labels)
negative_mse = grid_search.best_score_
rmse = np.sqrt(-negative_mse)
display_scores(rmse)

###############
# Excercise 2 #
###############
# Question: Try replacing GridSearchCV with RandomizedSearchCV.

param_distribs = {
    "kernel": ["linear", "rbf"],
    "C": reciprocal(20, 200000),
    "gamma": expon(scale=1.0),
}

random_search = RandomizedSearchCV(
    SVR(),
    n_jobs=20,
    param_distributions=param_distribs,
    n_iter=5,
    cv=2,
    scoring="neg_mean_squared_error",
    verbose=0,
    random_state=42,
)
random_search.fit(housing_prepared, housing_labels)
negative_mse = random_search.best_score_
rmse = np.sqrt(-negative_mse)
display_scores(rmse)

###############
# Excercise 3 #
###############
# Question: Try adding a transformer in the preparation pipeline to select only the most important attributes.

param_distribs = {
    "n_estimators": randint(low=1, high=200),
    "max_features": randint(low=1, high=8),
}

random_search = RandomizedSearchCV(
    RandomForestRegressor(),
    n_jobs=20,
    param_distributions=param_distribs,
    n_iter=5,
    cv=2,
    scoring="neg_mean_squared_error",
    verbose=0,
    random_state=42,
)
random_search.fit(housing_prepared, housing_labels)

features = random_search.best_estimator_.feature_importances_
top_feature_selector = TopFeatureSelector(features, 5)

housing_teste = top_feature_selector.fit_transform(housing_prepared)


forest_reg = RandomForestRegressor(n_estimators=10, random_state=42)
forest_reg.fit(housing_teste, housing_labels)

housing_predictions = random_search.predict(housing_prepared)
forest_mse = mean_squared_error(housing_labels, housing_predictions)
forest_rmse = np.sqrt(forest_mse)
print_with_separator("RS Random Forest RMSE: " + str(forest_rmse))

housing_predictions = forest_reg.predict(housing_teste)
forest_mse = mean_squared_error(housing_labels, housing_predictions)
forest_rmse = np.sqrt(forest_mse)
print_with_separator("Random Forest RMSE: " + str(forest_rmse))


###############
# Excercise 4 #
###############
# Question: Try creating a single pipeline that does the full data preparation plus the final prediction.
# Full Pipeline

pipeline = Pipeline(
    [
        ("data_preparation", full_pipeline),
        ("top_features", top_feature_selector),
        ("random_tree", forest_reg),
    ]
)

housing_predictions = pipeline.predict(housing)
forest_mse = mean_squared_error(housing_labels, housing_predictions)
forest_rmse = np.sqrt(forest_mse)
print_with_separator("Random Forest RMSE: " + str(forest_rmse))


###############
# Excercise 5 #
###############
# Question: Automatically explore some preparation options using GridSearchCV.

param_grid = [
    {
        "data_preparation__num__imputer__strategy": ["mean", "median", "most_frequent"],
        "top_features__k": list(
            range(1, len(random_search.best_estimator_.feature_importances_) + 1)
        ),
    }
]

grid_search_prep = GridSearchCV(
    pipeline,
    param_grid,
    cv=5,
    scoring="neg_mean_squared_error",
    verbose=0,
)
grid_search_prep.fit(housing, housing_labels)


print_with_separator(grid_search_prep.best_params_)
