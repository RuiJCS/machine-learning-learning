from random import random
from sklearn.datasets import fetch_openml
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import (
    confusion_matrix,
    recall_score,
    precision_score,
    f1_score,
    precision_recall_curve,
    roc_curve,
    roc_auc_score,
)
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from common import display_scores
from sklearn.model_selection import cross_val_score, GridSearchCV, train_test_split

mnist_original = fetch_openml("mnist_784", version=1)
mnist = mnist_original
print(len(mnist["data"]))
mnist_x = mnist["data"]
mnist_y = mnist["target"]
# spliter = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=525)
# split.split(data, data[attribute_to_divide])
mnist_x_train, mnist_x_test, mnist_y_train, mnist_y_test = train_test_split(
    mnist_x, mnist_y, test_size=0.15, random_state=525
)

param_grid = [
    {"kernel": ["linear"], "C": [10.0, 300.0, 3000.0, 30000.0]},
    {
        "kernel": ["rbf"],
        "C": [1.0, 3.0, 30.0, 300.0, 1000.0],
        "gamma": [0.01, 0.1, 0.3, 1.0, 3.0],
    },
]

grid_search = GridSearchCV(
    svm.SVC(), param_grid, n_jobs=10, cv=2, scoring="neg_mean_squared_error", verbose=1
)

grid_search.fit(mnist_x_train, mnist_y_train)

negative_mse = grid_search.best_score_
rmse = np.sqrt(-negative_mse)
display_scores(rmse)
