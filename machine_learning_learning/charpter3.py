from sklearn.datasets import fetch_openml
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import (
    confusion_matrix,
    recall_score,
    precision_score,
    f1_score,
    precision_recall_curve,
    roc_curve,
    roc_auc_score,
)
from sklearn.ensemble import RandomForestClassifier
from common import *

mnist_original = fetch_openml("mnist_784", version=1)
mnist = mnist_original
print(mnist.keys())

# Spliting the data between labels and features
X, y = mnist["data"], mnist["target"]


some_digit = X[0]
some_digit_image = some_digit.reshape(28, 28)

# plt.imshow(some_digit_image, cmap="binary")
# plt.axis("off")
# plt.show()

# labels are strings and need to be casted to numbers
y = y.astype(np.uint8)

X_train, y_train, X_test, y_test = X[:60000], y[:60000], X[60000:], y[:60000]

y_train_5 = y_train == 5
y_test_5 = y_test == 5

sgd_clf = SGDClassifier(random_state=42, n_jobs=4)
sgd_clf.fit(X_train, y_train_5)

# print(sgd_clf.predict([some_digit]))

# y_train_pred = cross_val_predict(sgd_clf, X_train, y_train_5, cv=3, n_jobs=4)

# conf_matrix = confusion_matrix(y_train_5, y_train_pred)

# print(conf_matrix)

# precision_score = precision_score(y_train_5, y_train_pred)

# recall_score = recall_score(y_train_5, y_train_pred)

# f1_score = f1_score(y_train_5, y_train_pred)

# print(
#     "Precision "
#     + str(precision_score)
#     + " Recall "
#     + str(recall_score)
#     + " F1 "
#     + str(f1_score)
# )


# y_scores = sgd_clf.decision_function([some_digit])
# threshold = 0
# y_some_digit_pred_zero = y_scores > threshold
# y_some_digit_pred_eight_k = y_scores > threshold
# print(
#     "y_scores = "
#     + str(y_scores)
#     + " y_some_digit_pred_zero = "
#     + str(y_some_digit_pred_zero)
#     + " y_y_some_digit_pred_eight_k = "
#     + str(y_some_digit_pred_eight_k)
# )

y_scores = cross_val_predict(
    sgd_clf, X_train, y_train_5, cv=3, method="decision_function", n_jobs=4
)

# precisions, recalls, thresholds = precision_recall_curve(y_train_5, y_scores)
# recall_90_precision = recalls[np.argmax(precisions >= 0.90)]
# threshold_90_precision = thresholds[np.argmax(precisions >= 0.90)]


# # plt.figure(figsize=(8, 4))  # Not shown
# plot_precision_recall_vs_threshold(precisions, recalls, thresholds)
# plt.plot(
#     [threshold_90_precision, threshold_90_precision], [0.0, 0.9], "r:"
# )  # Not shown
# plt.plot([-50000, threshold_90_precision], [0.9, 0.9], "r:")  # Not shown
# plt.plot(
#     [-50000, threshold_90_precision], [recall_90_precision, recall_90_precision], "r:"
# )  # Not shown
# plt.plot([threshold_90_precision], [0.9], "ro")  # Not shown
# plt.plot([threshold_90_precision], [recall_90_precision], "ro")  # Not shown
# plt.show()

# plt.figure(figsize=(8, 6))
# plot_precision_vs_recall(precisions, recalls)
# plt.plot([0.4368, 0.4368], [0.0, 0.9], "r:")
# plt.plot([0.0, 0.4368], [0.9, 0.9], "r:")
# plt.plot([0.4368], [0.9], "ro")
# plt.show()

# threshold_90_precision = thresholds[np.argmax(precisions >= 0.90)]
# y_train_pred_90 = y_scores >= threshold_90_precision
# print(
#     "threshold_90_precision "
#     + str(threshold_90_precision)
#     + " y_train_pred_90 "
#     + str(y_train_pred_90)
#     + " precision "
#     + str(precision_score(y_train_5, y_train_pred_90))
#     + " recall "
#     + str(recall_score(y_train_5, y_train_pred_90))
# )


fpr, tpr, thresholds = roc_curve(y_train_5, y_scores)

# plt.figure(figsize=(8, 6))  # Not shown
# plot_roc_curve(fpr, tpr)
# plt.plot([4.837e-3, 4.837e-3], [0.0, 0.4368], "r:")  # Not shown
# plt.plot([0.0, 4.837e-3], [0.4368, 0.4368], "r:")  # Not shown
# plt.plot([4.837e-3], [0.4368], "ro")  # Not shown
# save_fig("roc_curve_plot")  # Not shown
# plt.show()

# print("roc_auc_score" + str(roc_auc_score(y_train_5, y_scores)))


forest_clf = RandomForestClassifier(n_estimators=100, random_state=42)
y_probas_forest = cross_val_predict(
    forest_clf, X_train, y_train_5, cv=3, method="predict_proba"
)


y_scores_forest = y_probas_forest[:, 1]  # score = proba of positive class
fpr_forest, tpr_forest, thresholds_forest = roc_curve(y_train_5, y_scores_forest)

plt.figure(figsize=(8, 6))
plt.plot(fpr, tpr, "b:", linewidth=2, label="SGD")
plot_roc_curve(fpr_forest, tpr_forest, "Random Forest")
plt.plot([4.837e-3, 4.837e-3], [0.0, 0.4368], "r:")
plt.plot([0.0, 4.837e-3], [0.4368, 0.4368], "r:")
plt.plot([4.837e-3], [0.4368], "ro")
plt.plot([4.837e-3, 4.837e-3], [0.0, 0.9487], "r:")
plt.plot([4.837e-3], [0.9487], "ro")
plt.grid(True)
plt.legend(loc="lower right", fontsize=16)
plt.show()
