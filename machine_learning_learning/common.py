import os
import tarfile
import urllib
import pandas as pd
import matplotlib as plt
import joblib
from sklearn.model_selection import StratifiedKFold
from sklearn.base import clone
from scipy.ndimage import shift
import numpy as np
import urllib.request

DOWNLOAD_ROOT = "https://github.com/ageron/handson-ml2/raw/master/"
HOUSING_PATH = os.path.join("datasets", "housing")
TITANIC_PATH = os.path.join("datasets", "titanic")
HOUSING_URL = DOWNLOAD_ROOT + "datasets/housing/housing.tgz"
PRINT_SEPARATOR = "==========================================================================\n=========================================================================="


def save_model(my_model, name):
    joblib.dump(my_model, name)


def load_model(name):
    return joblib.load(name)


def fetch_housing_data(housing_url=HOUSING_URL, housing_path=HOUSING_PATH):
    os.makedirs(housing_path, exist_ok=True)
    tgz_path = os.path.join(housing_path, "housing.tgz")
    urllib.request.urlretrieve(housing_url, tgz_path)
    housing_tgz = tarfile.open(tgz_path)  # asd
    housing_tgz.extractall(path=housing_path)
    housing_tgz.close()


def load_housing_data(housing_path=HOUSING_PATH):
    csv_path = os.path.join(housing_path, "housing.csv")
    return pd.read_csv(csv_path)


def load_titanic_data(titanic_path=TITANIC_PATH):
    csv_train_path = os.path.join(titanic_path, "train.csv")
    csv_test_path = os.path.join(titanic_path, "test.csv")
    return pd.read_csv(csv_train_path), pd.read_csv(csv_test_path)


def print_with_separator(value_to_print):
    print(value_to_print)
    print(PRINT_SEPARATOR)


def display_scores(scores):
    print("Scores: ", scores)
    print("Mean: ", scores.mean())
    print_with_separator("Standard deviation: " + str(scores.std()))


def save_fig(fig_id, tight_layout=True, fig_extension="png", resolution=300):
    path = os.path.join(IMAGES_PATH, fig_id + "." + fig_extension)
    print("Saving figure", fig_id)
    if tight_layout:
        plt.tight_layout()
    plt.savefig(path, format=fig_extension, dpi=resolution)


def my_cross_validation(classifier, X, y, n):
    """Approximation to the sckitlearn cross validation"""
    skfolds = StratifiedKFold(n_splits=n, random_state=42)
    for train_index, test_index in skfolds.split(X, y):
        clone_clf = clone(classifier)
        X_train_folds = X[train_index]
        y_train_folds = X[train_index]
        X_test_fold = y[test_index]
        y_test_fold = y[test_index]

        clone_clf.fit(X_train_folds, y_train_folds)
        y_pred = clone_clf.predict(X_test_fold)
        n_correct = sum(y_pred == y_test_fold)
        print(n_correct / len(y_pred))


def plot_precision_recall_vs_threshold(precisions, recalls, thresholds):
    plt.plot(thresholds, precisions[:-1], "b--", label="Precision", linewidth=2)
    plt.plot(thresholds, recalls[:-1], "g-", label="Recall", linewidth=2)
    plt.legend(loc="center right", fontsize=16)  # Not shown in the book
    plt.xlabel("Threshold", fontsize=16)  # Not shown
    plt.grid(True)  # Not shown
    plt.axis([-50000, 50000, 0, 1])  # Not shown


def plot_precision_vs_recall(precisions, recalls):
    plt.plot(recalls, precisions, "b-", linewidth=2)
    plt.xlabel("Recall", fontsize=16)
    plt.ylabel("Precision", fontsize=16)
    plt.axis([0, 1, 0, 1])
    plt.grid(True)


def plot_roc_curve(fpr, tpr, label=None):
    plt.plot(fpr, tpr, linewidth=2, label=label)
    plt.plot([0, 1], [0, 1], "k--")  # dashed diagonal
    plt.axis([0, 1, 0, 1])  # Not shown in the book
    plt.xlabel("False Positive Rate (Fall-Out)", fontsize=16)  # Not shown
    plt.ylabel("True Positive Rate (Recall)", fontsize=16)  # Not shown
    plt.grid(True)  # Not shown


def shift_image(image, dx, dy):
    image = image.reshape((28, 28))
    shifted_image = shift(image, [dy, dx], cval=0, mode="constant")
    return shifted_image.reshape([-1])


def shift_dataset(dataset, labels, dx, dy):
    X_train_augmented = [image for image in dataset]
    y_train_augmented = [label for label in labels]

    for dx, dy in ((dy, 0), (-dy, 0), (0, dx), (0, -dx)):
        for image, label in zip(dataset, labels):
            X_train_augmented.append(shift_image(image, dx, dy))
            y_train_augmented.append(label)

    X_train_augmented = np.array(X_train_augmented)
    y_train_augmented = np.array(y_train_augmented)
    shuffle_idx = np.random.permutation(len(X_train_augmented))
    X_train_augmented = X_train_augmented[shuffle_idx]
    y_train_augmented = y_train_augmented[shuffle_idx]
    return X_train_augmented, y_train_augmented
