from sklearn.datasets import fetch_openml
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import SGDClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_predict, GridSearchCV
from sklearn.metrics import (
    confusion_matrix,
    recall_score,
    precision_score,
    f1_score,
    precision_recall_curve,
    roc_curve,
    roc_auc_score,
)
from sklearn.ensemble import RandomForestClassifier
from common import print_with_separator, shift_dataset
from sklearn.metrics import accuracy_score

mnist_original = fetch_openml("mnist_784", version=1)
mnist = mnist_original

param_grid = [
    {"n_estimators": [3, 10, 30], "max_features": [2, 4, 6, 8]},
    {"bootstrap": [False], "n_estimators": [3, 10], "max_features": [2, 3, 4]},
]

forest_reg = RandomForestClassifier()

grid_search = GridSearchCV(
    forest_reg, param_grid, n_jobs=4, cv=2, scoring="accuracy", return_train_score=True,
)

X, y = mnist["data"], mnist["target"]
X_train, y_train, X_test, y_test = X[:60000], y[:60000], X[60000:], y[60000:]
grid_search.fit(X_train, y_train)
mnist_predictions = grid_search.predict(X_test)
mnist_mse = mean_squared_error(y_test, mnist_predictions)
mnist_rmse = np.sqrt(mnist_mse)
print_with_separator(
    "Random Forest RMSE: "
    + str(mnist_rmse)
    + " accuracy: "
    + str(grid_search.best_score_)
)

knn_clf = KNeighborsClassifier(n_neighbors=3, n_jobs=8)

param_grid = [
    {
        "n_neighbors": [3]  # , 5],
        # "algorithm": ["auto", "ball_tree", "kd_tree", "brute"],
        # "weights": ["uniform", "distance"],
    },
]

grid_search = GridSearchCV(
    knn_clf, param_grid, n_jobs=8, cv=2, scoring="accuracy", return_train_score=True,
)

grid_search.fit(X_train, y_train)
mnist_predictions = grid_search.predict(X_test)
mnist_mse = mean_squared_error(y_test, mnist_predictions)
mnist_rmse = np.sqrt(mnist_mse)
print_with_separator(
    "KNeighborsClassifier RMSE: "
    + str(mnist_rmse)
    + " accuracy: "
    + str(accuracy_score(y_test, mnist_predictions))
)

X_train_augmented, y_train_augmented = shift_dataset(X_train, y_train, 1, 1)

knn_clf = KNeighborsClassifier(grid_search.best_params_)
knn_clf.fit(X_train, y_train)

y_pred = knn_clf.predict(X_test)
print_with_separator("Knn accuracy: " + str(accuracy_score(y_test, y_pred)))

