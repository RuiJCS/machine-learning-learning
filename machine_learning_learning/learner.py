from sklearn.model_selection import train_test_split, StratifiedShuffleSplit
from sklearn.base import BaseEstimator, TransformerMixin
import numpy as np


class Learner:
    "Class for the learning methods"
    train_set = 0
    test_set = 0

    def __init__(self, data, attribute_to_divide):
        # self.train_set, self.test_set = train_test_split(
        #     data, test_size=0.2, random_state=42
        # )
        split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
        for train_index, test_index in split.split(data, data[attribute_to_divide]):
            self.train_set = data.loc[train_index]
            self.test_set = data.loc[test_index]

        for set_ in (self.train_set, self.test_set):
            set_.drop(attribute_to_divide, axis=1, inplace=True)


def indices_of_top_k(arr, k):
    return np.sort(np.argpartition(np.array(arr), -k)[-k:])


class TopFeatureSelector(BaseEstimator, TransformerMixin):
    def __init__(self, feature_importances, k):
        self.feature_importances = feature_importances
        self.k = k

    def fit(self, X, y=None):
        self.feature_indices_ = indices_of_top_k(self.feature_importances, self.k)
        return self

    def transform(self, X):
        return X[:, self.feature_indices_]
